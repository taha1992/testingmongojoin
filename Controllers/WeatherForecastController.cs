﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using MongoDbJointureTeste.Services;
using MongoDbJointureTeste.Models;
using MongoDB.Bson;

namespace MongoDbJointureTeste.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class WeatherForecastController : ControllerBase
    {
        private static readonly string[] Summaries = new[]
        {
            "Freezing", "Bracing", "Chilly", "Cool", "Mild", "Warm", "Balmy", "Hot", "Sweltering", "Scorching"
        };

        private readonly ILogger<WeatherForecastController> _logger;
        private readonly CarService carService;


        public WeatherForecastController(
            ILogger<WeatherForecastController> logger,
            CarService carService
            )
        {
            _logger = logger;
            this.carService = carService;
        }

        [HttpGet]
        public ActionResult<string> Get()
        {

            return carService.GetWithBrandInfos();

        }
    }
}
