﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MongoDbJointureTeste.Models;
using Microsoft.Extensions.Configuration;
using MongoDB.Driver;
using MongoDB.Bson;

namespace MongoDbJointureTeste.Services
{
    public class CarService
    {

        private readonly IMongoCollection<Car> cars;
        private readonly IMongoCollection<Brand> brands;

        public CarService(IConfiguration config)
        {
            MongoClient client = new MongoClient(config.GetConnectionString("CarGalleryDb"));
            IMongoDatabase database = client.GetDatabase("CarGalleryDb");
            cars = database.GetCollection<Car>("Cars");
            brands = database.GetCollection<Brand>("Brands");
        }

        public List<Car> Get()
        {
            return cars.Find(car => true).ToList();
        }

        public string GetWithBrandInfos()
        {
            string result = cars.Aggregate()
                .Unwind<Car, UnwindedCar>(a => a.Brands)
                .Lookup<Brand, CarWithBrand>("Brands", "Brands._id", "_id", "brandsDetail")
                .Group(x => x.Id, g => new
                {
                    Name = g.Key,
                    brands = g.First().Brands,
                })
                .ToList()
                //.Project<CarWithBrand>(Builders<CarWithBrand>.Projection.Exclude("_id"))
                .ToString();
            //.Lookup<Car, Brand, CarWithBrand>(
            // brands,
            // x => x.Brands,
            //x => x.Id,
            //x => x.brandsDetail
            //).ToList();

            //.Lookup("Brands", "_id", "_id", "brandsDetail")
            //.Project(Builders<BsonDocument>.Projection.Exclude("_id"))
            // .ToList();
            //.lookup<car, brand, carwithbrand>(
            //    brands,
            //    x => x.brands,
            //    x => x.id,
            //    x => x.brandsdetail).tolist();

            return result;
        }

        public Car Get(string id)
        {
            return cars.Find(car => car.Id == id).FirstOrDefault();
        }

        public Car Create(Car car)
        {
            cars.InsertOne(car);
            return car;
        }

        public void Update(string id, Car carIn)
        {
            cars.ReplaceOne(car => car.Id == id, carIn);
        }

        public void Remove(Car carIn)
        {
            cars.DeleteOne(car => car.Id == carIn.Id);
        }

        public void Remove(string id)
        {
            cars.DeleteOne(car => car.Id == id);
        }
    }
}
