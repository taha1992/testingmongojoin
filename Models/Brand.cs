﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System.ComponentModel.DataAnnotations;

namespace MongoDbJointureTeste.Models
{
    public class Brand
    {

        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string Id { get; set; }

        [BsonElement("BrandName")]
        [Required]
        public string BrandName { get; set; }

        [BsonElement("BrandPopularity")]
       
        [Required]
        public string BrandPopularity { get; set; }
    }
}
