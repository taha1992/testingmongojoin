﻿using MongoDB.Bson;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MongoDbJointureTeste.Models
{
    public class CarWithBrand : Car
    {
        public Brand[] brandsDetail { get; set; }
    }
}
